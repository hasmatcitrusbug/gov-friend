$(document).ready(function(){ 

  $("#closemenu").click(function(){
    $(".st-menu-open").removeClass();
  });

  
  $(window).scroll(function() {
    if ($(this).scrollTop() >= 150) {        
        $('.return-to-top').addClass("display_show");    
    } else {
        $('.return-to-top').removeClass("display_show");   
    }
  });

  $('.return-to-top').click(function() {    
    $('body,html').animate({
        scrollTop : 0                       
    }, 0);
  });

});